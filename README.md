# nes_controller.sh

This is intended to be used with a cheap USB NES controller replica.

It uses "jstest" to react on pressed buttons.

You can e.g. control audio/mediaplayer with it.

Put it to e.g. `/usr/local/bin`.

# nes_controller.service

A unit file to be used with `/usr/local/bin/nes_controller.sh`.

Put it to `/etc/systemd/user` and execute `systemctl --user daemon-reload`.

Now you can enable/start it with `systemctl --user enable nes_controller.service` and `systemctl --user start nes_controller.service`.

# prerequisites

- python3
- python modules:
  * evdev
  * systemd

```
apt update
apt-get install python3 python3-evdev
pip3 install systemd-python
```
