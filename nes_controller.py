#!/usr/bin/env python3
# This is intended to be used with a cheap USB NES controller replica.
# It uses "jstest" to react on pressed buttons.
# You can e.g. control audio/mediaplayer with it.

# kudos: https://python-evdev.readthedocs.io/en/latest/apidoc.html

import asyncio, evdev, subprocess, sys, time
from systemd import journal

# configure your input device
js = evdev.InputDevice('/dev/input/event0')

# configure values according to debug log
bAVal = 289
bBVal = 288
bStartVal = 297
bSelectVal = 296

# debug js capabilities
print(js.capabilities(verbose=True,absinfo=True))

def log(message):
    print(message)
    journal.send(SYSLOG_IDENTIFIER="nes_controller", MESSAGE=message)

def left():
    log("left")
    subprocess.run(["mpc", "prev"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def right():
    log("right")
    subprocess.run(["mpc", "next"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def up():
    log("up")
    subprocess.run(["amixer", "-q", "-M", "set", "Headphone", "10%+"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def down():
    log("down")
    subprocess.run(["amixer", "-q", "-M", "set", "Headphone", "10%-"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def bA():
    log("A")

def bB():
    log("B")

def bStart():
    log("Start")
    subprocess.run(["mpc", "stop"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def bSelect():
    log("Select")
    subprocess.run(["mpc", "toggle"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

async def print_events(device):
    async for event in device.async_read_loop():

        # buttons
        if event.value == 1 and event.code == bAVal:
            bA()
        elif event.value == 1 and event.code == bBVal:
            bB()
        elif event.value == 1 and event.code == bStartVal:
            bStart()
        elif event.value == 1 and event.code == bSelectVal:
            bSelect()

        # axis
        if event.type == evdev.ecodes.EV_ABS:
            absevent = evdev.categorize(event)
            if evdev.ecodes.bytype[absevent.event.type][absevent.event.code] == 'ABS_X' and absevent.event.value == 0:
                left()
            elif evdev.ecodes.bytype[absevent.event.type][absevent.event.code] == 'ABS_X' and absevent.event.value == 255:
                right()
            elif evdev.ecodes.bytype[absevent.event.type][absevent.event.code] == 'ABS_Y' and absevent.event.value == 0:
                up()
            elif evdev.ecodes.bytype[absevent.event.type][absevent.event.code] == 'ABS_Y' and absevent.event.value == 255:
                down()

asyncio.ensure_future(print_events(js))

loop = asyncio.get_event_loop()
loop.run_forever()
